# Ressources BNAC OSM
Ressources à destination des utilisateurs de données BNAC dans QGIS.
Les données BNAC de France Métropolitaine sont produites régulièrement par Geovelo par conversion et export des données OpenStreetMap et peuvent être récupérer ici : https://www.data.gouv.fr/fr/datasets/amenagements-cyclables-france-metropolitaine/
Des exports sont aussi récupérables sur le service GeoDataMine, ils sont réalisés avec un autre modèle d'export, peut-être moins complet que celui de Geovelo : https://geodatamine.fr/